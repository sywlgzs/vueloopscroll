# vueloopscroll 无限循环滚动VUE插件

## Project setup


###使用 

```
1、在 main.js中引入

import VueLoopScroll from 'vueloopscroll'
import 'vueloopscroll/lib/vueloopscroll.css';
Vue.use(VueLoopScroll)

2、在项目中使用
<style scoped="scoped" lang="scss">
	.scrolltop{ background:#f4f4f4; margin-top:20px;
	  &.s1{ li{ line-height:50px;} }
		&.s2{ 
			.vue-loop-scroll-box{height:200px;}
		}
	}
</style>
<template>
	<div>
		
		<div class="scrolltop">
			<vue-loop-scroll  direction="up" :speed="30" :index=0 :mouseStop="false">
					 <li v-for="(item,index) in list">{{item.title}}</li>
				</vue-loop-scroll>
		</div>
		<div class="scrolltop s1">
					<vue-loop-scroll direction="left" :speed="30" :index=1 :mouseStop="false">
						<li v-for="(item,index) in list">{{item.title}}</li>
					</vue-loop-scroll>
		</div>
		<div class="scrolltop s2">
				<vue-loop-scroll  direction="up" :speed="30" :index=2 :mouseStop="true">
						<li v-for="(item,index) in list">{{item.title}}</li>
					</vue-loop-scroll>
		</div>
		
	</div>
</template>

<script>
	 export default{
		  data(){
				return{
					list:[
						{title:'向上滚动 134***5610'},
						{title:'向上滚动 134***5612'},
						{title:'向上滚动 134***5613'},
						{title:'向上滚动 134***5614'},
						{title:'向上滚动 134***5615'},
						{title:'向上滚动 134***5616'},
						{title:'向上滚动 134***5617'},
						{title:'向上滚动 134***5618'},
						{title:'向上滚动 134***5619'},
						{title:'向上滚动 134***5615'},
					]
				}
			}
	 }
</script>


direction 方向 [left 向左 up 向上] 默认向左
speed 滚动速度 数值越小滚动越快 默认50
index 页面引入多个时改变一下数值 例如第二个 填1 
mouseStop 鼠标移入是否停止滚动 true 或 false 默认false


```
![Image text](https://img-blog.csdnimg.cn/20201114170025896.gif)
